import 'package:example_flutter_app/views/home/home_page.dart';
import 'package:example_flutter_app/views/login/login_page.dart';
import 'package:example_flutter_app/views/posts/post_detail_page.dart';
import 'package:example_flutter_app/views/posts/post_page.dart';
import 'package:example_flutter_app/views/register/register_page.dart';
import 'package:example_flutter_app/views/splash/splash_page.dart';
import 'package:flutter/material.dart';

class AppRoute {
  static const home = "/";
  static const splash = "/splash";
  static const login = "/login";
  static const posts = "/posts";
  static const postDetail = "/post-detail";
  static const register = "/register";

  static const initialRoute = AppRoute.splash;

  static Map<String, WidgetBuilder> routes = {
    AppRoute.splash: ((context) => const SplashPage()),
    AppRoute.login: (context) => const LoginPage(),
    AppRoute.home: ((context) => const HomePage()),
    AppRoute.posts: ((context) => const PostPage()),
    AppRoute.postDetail: ((context) => const PostDetailPage()),
    AppRoute.register: (context) => const RegisterPage(),
  };
}
