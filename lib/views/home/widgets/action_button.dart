import 'package:example_flutter_app/routes.dart';
import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  const ActionButton({super.key});

  void handleClick(BuildContext context) {
    Navigator.of(context).pushNamed(AppRoute.posts);
  }

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () => handleClick(context),
      child: const Text("See All Posts"),
    );
  }
}
