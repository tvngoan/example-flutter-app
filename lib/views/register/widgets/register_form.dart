import 'package:example_flutter_app/providers/register_provider.dart';
import 'package:example_flutter_app/views/shared/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegisterForm extends StatelessWidget {
  const RegisterForm({super.key});

  @override
  Widget build(BuildContext context) {
    final registerProvider = Provider.of<RegisterProvider>(context);

    return Form(
      key: registerProvider.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomTextField(
              label: "Display Name",
              hintText: "Enter your display name",
              onSaved: registerProvider.saveDisplayName,
              validator: registerProvider.validateDisplayName,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomTextField(
              label: "Email",
              hintText: "Enter your email",
              onSaved: registerProvider.saveEmail,
              validator: registerProvider.validateEmail,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomTextField(
              label: "Password",
              hintText: "Enter your password",
              onSaved: registerProvider.savePassword,
              validator: registerProvider.validatePassword,
            ),
          ),
          if (registerProvider.isLoading)
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: LinearProgressIndicator(),
            ),
          if (registerProvider.isError)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("${registerProvider.error}"),
            ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: registerProvider.isLoading
                  ? null // Disable button
                  : registerProvider.handleRegister,
              child: const Text("Register"),
            ),
          ),
        ],
      ),
    );
  }
}
