import 'package:example_flutter_app/providers/login_provider.dart';
import 'package:example_flutter_app/views/shared/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    final loginProvider = Provider.of<LoginProvider>(context);

    return Form(
      key: loginProvider.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomTextField(
              label: "Email",
              hintText: "Enter your email",
              onSaved: loginProvider.saveEmail,
              validator: MultiValidator([
                RequiredValidator(errorText: "Email is required"),
                EmailValidator(errorText: "Invalid email format"),
              ]),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomTextField(
              label: "Password",
              hintText: "Enter your password",
              obscureText: true,
              onSaved: loginProvider.savePassword,
              validator: RequiredValidator(errorText: "Password is required"),
            ),
          ),
          if (loginProvider.isLoading)
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: LinearProgressIndicator(),
            ),
          if (loginProvider.isError)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("${loginProvider.error}"),
            ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: loginProvider.isLoading
                  ? null // Disable button
                  : loginProvider.handleLogin,
              child: const Text("Login"),
            ),
          )
        ],
      ),
    );
  }
}
