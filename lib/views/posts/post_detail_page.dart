import 'package:example_flutter_app/models/posts/post.dart';
import 'package:example_flutter_app/providers/post_detail_provider.dart';
import 'package:example_flutter_app/views/posts/widgets/post_detail_content.dart';
import 'package:example_flutter_app/views/posts/widgets/post_detail_header.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostDetailPage extends StatelessWidget {
  const PostDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    final post = ModalRoute.of(context)!.settings.arguments as Post;

    return ChangeNotifierProvider(
      create: (_) => PostDetailProvider(id: post.id!),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Post Detail"),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.edit),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              PostDetailHeader(post: post),
              const PostDetailContent(),
            ],
          ),
        ),
      ),
    );
  }
}
