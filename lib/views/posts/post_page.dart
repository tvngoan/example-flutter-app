import 'package:example_flutter_app/providers/post_list_provider.dart';
import 'package:example_flutter_app/views/posts/widgets/post_filter.dart';
import 'package:example_flutter_app/views/posts/widgets/post_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostPage extends StatelessWidget {
  const PostPage({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => PostListProvider(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("All Posts"),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.add),
            )
          ],
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: const [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 18.0),
              child: PostFilter(),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 18.0),
                child: PostList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
