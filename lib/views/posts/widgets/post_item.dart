import 'package:example_flutter_app/models/posts/post.dart';
import 'package:example_flutter_app/providers/post_list_provider.dart';
import 'package:example_flutter_app/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostItem extends StatelessWidget {
  final Post post;
  final int index;

  const PostItem({
    super.key,
    required this.post,
    required this.index,
  });

  void handleItemClick(BuildContext context) {
    Navigator.of(context).pushNamed(
      AppRoute.postDetail,
      arguments: post,
    );
  }

  @override
  Widget build(BuildContext context) {
    final postList = Provider.of<PostListProvider>(context, listen: false);

    return InkWell(
      onTap: () => handleItemClick(context),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            Stack(
              children: [
                SizedBox(
                  height: 160,
                  width: double.infinity,
                  child: Hero(
                    tag: "post-image-${post.id}",
                    child: Image.network(
                      post.thumbnail.toString(),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                  right: 8,
                  top: 4,
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.black.withOpacity(.6),
                    ),
                    child: IconButton(
                      onPressed: () => postList.removeAt(index),
                      color: Colors.white,
                      icon: const Icon(Icons.close),
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            ListTile(
              leading: CircleAvatar(
                child: Text(post.id.toString()),
              ),
              title: Text(post.title ?? ""),
              subtitle: Text(
                post.desc ?? '',
                maxLines: 2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
