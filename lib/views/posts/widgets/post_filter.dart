import 'package:example_flutter_app/providers/post_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostFilter extends StatelessWidget {
  const PostFilter({super.key});

  @override
  Widget build(BuildContext context) {
    final posts = Provider.of<PostListProvider>(context);

    return Row(
      children: [
        SizedBox(
          width: 200,
          child: TextField(
            decoration: const InputDecoration(
              icon: Icon(Icons.search),
              hintText: "Search",
              border: InputBorder.none,
              isDense: true,
            ),
            onChanged: posts.setSearch,
          ),
        ),
        const Expanded(child: Text('')),
        ButtonBar(
          children: [
            IconButton(
              onPressed: posts.prevPage,
              iconSize: 18,
              icon: const Icon(Icons.arrow_back_ios),
            ),
            Container(
              color: Colors.grey[200],
              padding: const EdgeInsets.all(5),
              child: Text(posts.page.toString()),
            ),
            IconButton(
              onPressed: posts.nextPage,
              iconSize: 18,
              icon: const Icon(Icons.arrow_forward_ios),
            ),
          ],
        )
      ],
    );
  }
}
