import 'package:example_flutter_app/providers/post_list_provider.dart';
import 'package:example_flutter_app/views/posts/widgets/post_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostList extends StatelessWidget {
  const PostList({super.key});

  @override
  Widget build(BuildContext context) {
    final posts = Provider.of<PostListProvider>(context);

    if (posts.isLoading && posts.isEmpty) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    if (posts.isEmpty) {
      return const Center(
        child: Text("No data"),
      );
    }

    return RefreshIndicator(
      onRefresh: posts.refresh,
      child: CustomScrollView(
        slivers: [
          if (posts.isAdding || posts.isDeleting || posts.isError)
            SliverAppBar(
              title: posts.isDeleting
                  ? const Text("Removing a post...")
                  : (posts.isAdding
                      ? const Text("Adding a new post...")
                      : Text(
                          "${posts.error}",
                          style: const TextStyle(color: Colors.red),
                        )),
              pinned: true,
              primary: false,
              automaticallyImplyLeading: false,
              titleTextStyle: Theme.of(context)
                  .textTheme
                  .bodyText2
                  ?.copyWith(color: Colors.grey),
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              toolbarHeight: 24,
            ),
          SliverGrid(
            delegate: SliverChildListDelegate(
              List.generate(
                posts.items.length,
                (index) => PostItem(
                  post: posts.items[index],
                  index: index,
                ),
              ),
            ),
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 400,
              mainAxisExtent: 260,
              mainAxisSpacing: 18,
              crossAxisSpacing: 18,
            ),
          ),
        ],
      ),
    );
  }
}
