import 'package:example_flutter_app/models/posts/post.dart';
import 'package:flutter/material.dart';

class PostDetailHeader extends StatelessWidget {
  final Post post;

  const PostDetailHeader({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 300,
          width: double.infinity,
          child: Hero(
            tag: "post-image-${post.id}",
            child: Image.network(
              post.thumbnail!.toString(),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 8),
          child: Text(
            post.createdAt.toString(),
            style: Theme.of(context).textTheme.overline,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(18.0, 0, 18.0, 8),
          child: Text(
            post.title ?? "",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
      ],
    );
  }
}
