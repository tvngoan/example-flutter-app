import 'package:example_flutter_app/providers/post_detail_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostDetailContent extends StatelessWidget {
  const PostDetailContent({super.key});

  @override
  Widget build(BuildContext context) {
    final postDetail = Provider.of<PostDetailProvider>(context);

    if (postDetail.isLoading) {
      return const Center(
        child: Padding(
          padding: EdgeInsets.all(18.0),
          child: CircularProgressIndicator(),
        ),
      );
    }

    if (postDetail.isError) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Text("Error: ${postDetail.error}"),
        ),
      );
    }

    if (postDetail.isEmpty) {
      return const Center(
        child: Padding(
          padding: EdgeInsets.all(18.0),
          child: Text("No data"),
        ),
      );
    }

    final data = postDetail.data!;

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: Text(
            data.desc ?? '',
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
      ],
    );
  }
}
