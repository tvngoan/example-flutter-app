import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final String? Function(String?)? validator;
  final void Function(String?)? onSaved;
  final String? initialValue;
  final String label;
  final String hintText;
  final bool obscureText;

  const CustomTextField({
    super.key,
    this.validator,
    this.onSaved,
    this.initialValue,
    this.obscureText = false,
    required this.label,
    required this.hintText,
  });

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _isValid = true;
  final _ctrl = TextEditingController();

  String? onValidate(String? val) {
    if (widget.validator != null) {
      final result = widget.validator!(val);
      setState(() {
        _isValid = result == null;
      });
      return result;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(widget.label),
        ),
        TextFormField(
          validator: onValidate,
          controller: _ctrl,
          initialValue: widget.initialValue,
          onSaved: widget.onSaved,
          obscureText: widget.obscureText,
          decoration: InputDecoration(
            hintText: widget.hintText,
            border: const OutlineInputBorder(),
            suffixIcon: _ctrl.text.isEmpty
                ? null
                : (_isValid
                    ? const Icon(
                        Icons.check,
                        color: Colors.green,
                      )
                    : const Icon(
                        Icons.close,
                        color: Colors.red,
                      )),
          ),
        ),
      ],
    );
  }
}
