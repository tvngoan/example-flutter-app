import 'dart:convert';

import 'package:example_flutter_app/helpers/api_client.dart';
import 'package:example_flutter_app/models/shared/api_exception.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonEnum(fieldRename: FieldRename.screamingSnake)
enum UserRole {
  admin,
  member,
}

@JsonSerializable()
class User {
  @JsonKey(name: '_id')
  String? id;
  String? displayName;
  String? email;
  String? password;
  UserRole? role;
  String? avatar;
  DateTime? createdAt;
  DateTime? updatedAt;

  User({
    this.id,
    this.displayName,
    this.email,
    this.password,
    this.role,
    this.avatar,
    this.createdAt,
    this.updatedAt,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  static Future<User> fetchMyProfile() async {
    final uri = APIClient.main.createURI('/api/auth/me');
    final res = await APIClient.main.get(uri);
    final json = jsonDecode(res.body);
    if (res.statusCode == 200) {
      return User.fromJson(json);
    } else {
      throw ApiException.fromJson(json);
    }
  }
}
