class ApiException implements Exception {
  final dynamic message;
  final int? code;

  String get messageString =>
      message is List ? (message as List).join('. ') : message;

  List<String> get messageList => message is List
      ? (message as List).map((e) => e.toString()).toList()
      : [message ?? ""];

  const ApiException(
    this.message, {
    this.code,
  });

  @override
  String toString() => 'ApiException ($code) ${messageList.join(", ")}';

  factory ApiException.fromJson(Map<String, dynamic> json) {
    return ApiException(
      json["message"] ?? "",
      code: json["status"] ?? json["statusCode"] ?? 0,
    );
  }
}
