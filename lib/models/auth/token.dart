import 'dart:convert';

import 'package:example_flutter_app/helpers/api_client.dart';
import 'package:example_flutter_app/models/shared/api_exception.dart';
import 'package:json_annotation/json_annotation.dart';

part 'token.g.dart';

@JsonSerializable()
class Token {
  @JsonKey(name: 'access_token')
  String accessToken;

  @JsonKey(name: 'refresh_token')
  String refreshToken;

  Token({required this.accessToken, required this.refreshToken});

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  Map<String, dynamic> toJson() => _$TokenToJson(this);

  Future<Token> refresh() async {
    final uri = APIClient.main.createURI('/api/auth/refresh-token');
    final res = await APIClient.main.post(
      uri,
      body: jsonEncode({
        "token": refreshToken,
      }),
    );
    final json = jsonDecode(res.body);
    if (res.statusCode == 201) {
      return Token.fromJson(json);
    } else {
      throw ApiException.fromJson(json);
    }
  }
}
