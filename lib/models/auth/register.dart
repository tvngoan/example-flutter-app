import 'dart:convert';
import 'package:example_flutter_app/helpers/api_client.dart';
import 'package:example_flutter_app/models/shared/api_exception.dart';
import 'package:json_annotation/json_annotation.dart';

part 'register.g.dart';

@JsonSerializable()
class Register {
  String displayName;
  String email;
  String password;

  Register({
    required this.displayName,
    required this.password,
    required this.email,
  });

  factory Register.fromJson(Map<String, dynamic> json) =>
      _$RegisterFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterToJson(this);

  Future<void> register() async {
    final uri = APIClient.main.createURI('/api/auth/register');
    final res = await APIClient.main.post(
      uri,
      body: jsonEncode(this),
    );
    if (res.statusCode != 201) {
      final json = jsonDecode(res.body);
      throw ApiException.fromJson(json);
    }
  }
}
