import 'dart:convert';
import 'package:example_flutter_app/helpers/api_client.dart';
import 'package:example_flutter_app/models/auth/token.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:example_flutter_app/models/shared/api_exception.dart';

part 'login.g.dart';

@JsonSerializable()
class Login {
  String email;
  String password;

  Login({required this.email, required this.password});

  factory Login.fromJson(Map<String, dynamic> json) => _$LoginFromJson(json);

  Map<String, dynamic> toJson() => _$LoginToJson(this);

  Future<Token> login() async {
    final uri = APIClient.main.createURI('/api/auth/login');
    final res = await APIClient.main.post(
      uri,
      body: jsonEncode(this),
    );
    final json = jsonDecode(res.body);
    if (res.statusCode == 201) {
      return Token.fromJson(json);
    }
    throw ApiException.fromJson(json);
  }
}
