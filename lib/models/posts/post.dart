import 'dart:convert';
import 'package:example_flutter_app/helpers/api_client.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:remote_data_provider/remote_list.dart';

part 'post.g.dart';

@JsonSerializable()
class Post {
  String? id;
  String? title;
  String? desc;
  Uri? thumbnail;
  DateTime? createdAt;

  Post({
    this.title,
    this.id,
    this.desc,
    this.thumbnail,
    this.createdAt,
  });

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);
  Map<String, dynamic> toJson() => _$PostToJson(this);

  static Future<RemoteList<Post>> fetchAll({
    int page = 1,
    int limit = 10,
    String search = '',
  }) async {
    final uri = APIClient.public.createURI('/api/posts', params: {
      "page": "$page",
      "limit": "$limit",
      "search": search,
    });

    final res = await APIClient.public.get(uri);
    final jsonList = jsonDecode(res.body) as List<dynamic>;

    final items = jsonList.map((e) => Post.fromJson(e)).toList();

    return RemoteList(
      items: items,
      totalItem: 100,
      search: search,
      page: page,
      pageSize: limit,
    );
  }

  static Future<Post> getDetail(String id) async {
    final uri = APIClient.public.createURI('/api/posts/$id');
    final res = await APIClient.public.get(uri);
    final data = jsonDecode(res.body) as Map<String, dynamic>;
    return Post.fromJson(data);
  }

  static Future<bool> delete(String id) async {
    final uri = APIClient.public.createURI('/api/posts/$id');
    final res = await APIClient.public.delete(uri);
    return res.statusCode == 200;
  }

  static Future<Post> update(Post post) async {
    final uri = APIClient.public.createURI('/api/posts/${post.id}');
    final res = await APIClient.public.put(
      uri,
      body: post.toJson()..remove('id'),
    );
    final data = jsonDecode(res.body) as Map<String, dynamic>;
    return Post.fromJson(data);
  }

  static Future<Post> create(Post newPost) async {
    final uri = APIClient.public.createURI('/api/posts');

    final res = await APIClient.public.post(
      uri,
      body: jsonEncode(newPost.toJson()),
    );

    final data = jsonDecode(res.body) as Map<String, dynamic>;
    return Post.fromJson(data);
  }
}
