// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Post _$PostFromJson(Map<String, dynamic> json) => Post(
      title: json['title'] as String?,
      id: json['id'] as String?,
      desc: json['desc'] as String?,
      thumbnail: json['thumbnail'] == null
          ? null
          : Uri.parse(json['thumbnail'] as String),
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
    );

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'desc': instance.desc,
      'thumbnail': instance.thumbnail?.toString(),
      'createdAt': instance.createdAt?.toIso8601String(),
    };
