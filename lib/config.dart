import 'package:flutter/material.dart';

class AppConfig {
  static const appName = 'Example App';
  static const mainApi = 'https://example-api.go.drimaesvn.com';
  static const publicApi = 'https://640cb08d94ce1239b0b366da.mockapi.io';

  static ThemeData theme = ThemeData(
    primarySwatch: Colors.blue,
  );
}
