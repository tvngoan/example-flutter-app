import 'package:example_flutter_app/models/posts/post.dart';
import 'package:remote_data_provider/data_list_provider.dart';
import 'package:remote_data_provider/remote_list.dart';

class PostListProvider extends DataListProvider<Post> {
  @override
  Future<RemoteList<Post>> onFetch() async {
    return Post.fetchAll(
      page: page,
      limit: pageSize,
      search: search ?? '',
    );
  }

  @override
  Future<Post> onCreate(Post newItem) {
    return Post.create(newItem);
  }

  @override
  Future<bool> onDelete(Post item) {
    return Post.delete(item.id!);
  }

  @override
  Future<Post> onUpdate(Post newData) {
    return Post.update(newData);
  }
}
