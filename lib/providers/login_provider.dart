import 'package:example_flutter_app/models/auth/login.dart';
import 'package:example_flutter_app/models/auth/token.dart';
import 'package:example_flutter_app/providers/app_provider.dart';
import 'package:example_flutter_app/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remote_data_provider/basic_data_provider.dart';

class LoginProvider extends BasicDataProvider<Token> {
  final BuildContext context;
  final formKey = GlobalKey<FormState>();

  final Login _loginData = Login(
    email: '',
    password: '',
  );

  LoginProvider(this.context) : super(manual: true, awaitListener: true);

  void saveEmail(String? val) {
    _loginData.email = val ?? '';
  }

  void savePassword(String? val) {
    _loginData.password = val ?? '';
  }

  void handleLogin() {
    final isValid = formKey.currentState!.validate();
    if (isValid) {
      formKey.currentState!.save();
      fetch();
    }
  }

  @override
  Future<Token> onFetch() async {
    return _loginData.login();
  }

  @override
  Future<void> onFetchCompleted(Token data) async {
    final appProvider = context.read<AppProvider>();

    await appProvider.login(data);

    // ignore: use_build_context_synchronously
    Navigator.of(context).popUntil(ModalRoute.withName(AppRoute.home));
  }
}
