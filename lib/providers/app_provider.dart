import 'package:example_flutter_app/helpers/api_client.dart';
import 'package:example_flutter_app/models/auth/token.dart';
import 'package:example_flutter_app/models/users/user.dart';
import 'package:example_flutter_app/routes.dart';
import 'package:flutter/material.dart';

class AppProvider extends ChangeNotifier {
  GlobalKey<NavigatorState> navKey;

  bool _initialized = false;
  bool _isAuth = false;
  User? _currentUser;

  bool get initialized => _initialized;
  bool get isAuth => _isAuth;
  User? get currentUser => _currentUser;

  BuildContext? get context => navKey.currentContext;

  AppProvider(this.navKey) {
    debugPrint(">> Init app provider.");
    _initApp();
  }

  void _initApp() async {
    await APIClient.init();

    _isAuth = APIClient.isAuth();
    if (_isAuth) {
      try {
        _currentUser = await User.fetchMyProfile();
        APIClient.onLogout = logout;
      } catch (e) {
        _isAuth = false;
        debugPrint(">> fetchMyProfile error: $e");
      }
    }

    _initialized = true;
    notifyListeners();

    // Enjoy splash screen for a while
    await Future.delayed(const Duration(seconds: 2));

    if (!_isAuth) {
      Navigator.of(context!).pushNamed(AppRoute.login);
    } else {
      Navigator.of(context!).popUntil(ModalRoute.withName(AppRoute.home));
    }
  }

  Future<void> login(Token token) async {
    APIClient.main
      ..setAuthData(token.toJson())
      ..setHeaders({
        "Authorization": "Bearer ${token.accessToken}",
      });
    try {
      _isAuth = true;
      _currentUser = await User.fetchMyProfile();
      APIClient.onLogout = logout;
    } catch (e) {
      _isAuth = false;
      debugPrint(">> fetchMyProfile error: $e");
    }
    notifyListeners();
  }

  void logout() {
    if (!_isAuth) return;

    APIClient.main
      ..setAuthData(null)
      ..setHeaders({
        "Authorization": "",
      });
    APIClient.onLogout = null;

    _isAuth = false;
    notifyListeners();

    Navigator.of(context!).pushNamed(AppRoute.login);
  }
}
