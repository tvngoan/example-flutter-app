import 'package:example_flutter_app/models/auth/register.dart';
import 'package:example_flutter_app/routes.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:remote_data_provider/basic_data_provider.dart';

class RegisterProvider extends BasicDataProvider {
  final BuildContext context;
  final formKey = GlobalKey<FormState>();

  final _input = Register(
    displayName: '',
    email: '',
    password: '',
  );

  RegisterProvider(this.context) : super(manual: true, awaitListener: true);

  void saveDisplayName(String? val) {
    _input.displayName = val ?? '';
  }

  void saveEmail(String? val) {
    _input.email = val ?? '';
  }

  void savePassword(String? val) {
    _input.password = val ?? '';
  }

  String? validateDisplayName(String? val) {
    final validator = MultiValidator([
      RequiredValidator(errorText: 'Display name is required'),
      MinLengthValidator(6, errorText: 'Display name must longer than 6 chars'),
    ]);
    return validator.call(val);
  }

  String? validateEmail(String? val) {
    final validator = MultiValidator([
      RequiredValidator(errorText: 'Email is required'),
      EmailValidator(errorText: 'Email format is invalid'),
    ]);
    return validator.call(val);
  }

  String? validatePassword(String? val) {
    final validator = MultiValidator([
      RequiredValidator(errorText: 'Password is required'),
      MinLengthValidator(8, errorText: 'Password must longer than 8 chars'),
      PatternValidator(
        r'(?=.*[#?!@$%^&*-])',
        errorText: 'Password must contains special character',
      ),
      PatternValidator(
        r'(?=.*[a-z])',
        errorText: 'Password must contains lowercase letter',
      ),
      PatternValidator(
        r'(?=.*[A-Z])',
        errorText: 'Password must contains uppercase letter',
      ),
      PatternValidator(
        r'(?=.*\d)',
        errorText: 'Password must contains number ',
      ),
    ]);
    return validator.call(val);
  }

  void handleRegister() {
    final isValid = formKey.currentState!.validate();
    if (isValid) {
      formKey.currentState!.save();
      fetch();
    }
  }

  @override
  Future onFetch() async {
    return _input.register();
  }

  @override
  Future<void> onFetchCompleted(data) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => SimpleDialog(
        title: const Text("Register success"),
        contentPadding: const EdgeInsets.all(24),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: () => Navigator.of(context)
                    .popUntil(ModalRoute.withName(AppRoute.login)),
                child: const Text("Login"),
              ),
            ],
          )
        ],
      ),
    );
  }
}
