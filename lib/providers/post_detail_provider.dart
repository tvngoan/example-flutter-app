import 'package:example_flutter_app/models/posts/post.dart';
import 'package:remote_data_provider/basic_data_provider.dart';

class PostDetailProvider extends BasicDataProvider<Post> {
  final String _id;

  PostDetailProvider({required String id})
      : _id = id,
        super();

  @override
  Future<Post> onFetch() {
    return Post.getDetail(_id);
  }

  @override
  Future<Post> onUpdate(Post newData) {
    return Post.update(newData);
  }

  @override
  Future<bool> onDelete() {
    return Post.delete(data!.id!);
  }
}
