import 'package:example_flutter_app/config.dart';
import 'package:example_flutter_app/models/auth/token.dart';
import 'package:extended_http/extended_http.dart';
import 'package:flutter/foundation.dart';
// ignore: depend_on_referenced_packages
import 'package:http/http.dart';

const authPaths = ['/api/auth/refresh-token', '/api/auth/login'];

class APIClient {
  static final public = ExtendedHttp('public');
  static final main = ExtendedHttp('main');

  static Future<void> init() async {
    public.config(
      baseURL: AppConfig.publicApi,
      logURL: true,
      logRespondBody: true,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    );

    await public.ensureInitialized();

    main
      ..config(
        baseURL: AppConfig.mainApi,
        logURL: true,
        logRespondBody: true,
        sendDebugId: kDebugMode,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
      )
      ..onError = _onHttpError
      ..shouldRetry = _shouldRetry
      ..onUnauthorized = _handleUnauthorized;

    await main.ensureInitialized();
  }

  static bool isAuth() {
    final authData = main.authData;
    debugPrint(">> Local auth data: $authData");
    if (authData != null) {
      final token = Token.fromJson(authData);
      APIClient.main.setHeaders({
        "Authorization": "Bearer ${token.accessToken}",
      });
      return true;
    }

    return false;
  }

  static bool _onHttpError(Object e, StackTrace s) {
    debugPrint("HTTP ERROR: $e");
    debugPrintStack(stackTrace: s);
    if ("${e.runtimeType}" == "_ClientSocketException") {
      // No network connection
    }
    return false;
  }

  static bool _shouldRetry(BaseResponse response) {
    if (response.statusCode == 503) {
      return true;
    }
    if (response.statusCode == 401 && main.authData != null) {
      if (!authPaths.contains(response.request?.url.path)) {
        return true;
      }
    }
    return false;
  }

  static Function()? onLogout;

  static Future<void> _handleUnauthorized(
      Map<String, dynamic>? authData) async {
    final authData = main.authData;
    if (authData == null) return;
    try {
      debugPrint(">> Try refresh token");
      final token = Token.fromJson(authData);
      final newToken = await token.refresh();
      main
        ..setAuthData(newToken.toJson())
        ..setHeaders({
          "Authorization": "Bearer ${token.accessToken}",
        });
    } catch (e) {
      debugPrint(">> Refresh token error: $e");
      main.setAuthData(null);
      onLogout?.call();
    }
  }
}
