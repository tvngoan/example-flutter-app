import 'package:example_flutter_app/config.dart';
import 'package:example_flutter_app/providers/app_provider.dart';
import 'package:example_flutter_app/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  final navKey = GlobalKey<NavigatorState>();

  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AppProvider(navKey),
          lazy: false,
        ),
      ],
      child: MaterialApp(
        title: AppConfig.appName,
        theme: AppConfig.theme,
        routes: AppRoute.routes,
        initialRoute: AppRoute.initialRoute,
        navigatorKey: navKey,
      ),
    );
  }
}
